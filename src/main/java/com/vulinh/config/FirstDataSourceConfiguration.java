package com.vulinh.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
    basePackages = "com.vulinh.repository.first",
    entityManagerFactoryRef = "first-entity-manager-factory-bean",
    transactionManagerRef = "first-transaction-manager"
)
public class FirstDataSourceConfiguration {

    @Value("${app.datasource.first.driver-class-name}")
    private String driverClassName;

    @Value("${app.datasource.first.url}")
    private String url;

    @Value("${app.datasource.first.username}")
    private String username;

    @Value("${app.datasource.first.password}")
    private String password;

    @Primary
    @Bean("first-datasource")
    public DataSource dataSource() {
        DataSourceProperties dataSourceProperties = new DataSourceProperties();
        dataSourceProperties.setDriverClassName(driverClassName);
        dataSourceProperties.setUrl(url);
        dataSourceProperties.setUsername(username);
        dataSourceProperties.setPassword(password);
        dataSourceProperties.setType(HikariDataSource.class);
        return dataSourceProperties.initializeDataSourceBuilder().build();
    }

    @Primary
    @Bean("first-entity-manager-factory-bean")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(
        EntityManagerFactoryBuilder entityManagerFactoryBuilder
    ) {
        return entityManagerFactoryBuilder.dataSource(dataSource())
                                          // Where your entity class are
                                          .packages("com.vulinh.entity.first")
                                          // A name for your newborn kid
                                          .persistenceUnit("first")
                                          .build();
    }

    @Primary
    @Bean("first-transaction-manager")
    public PlatformTransactionManager platformTransactionManager(
        // Refer to the first datasource's EntityManagerFactory bean
        @Qualifier("first-entity-manager-factory-bean") EntityManagerFactory entityManagerFactory
    ) {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
