package com.vulinh.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
    basePackages = "com.vulinh.repository.second",
    entityManagerFactoryRef = "second-entity-manager-factory-bean",
    transactionManagerRef = "second-transaction-manager"
)
public class SecondDataSourceConfiguration {

    @Value("${app.datasource.second.driver-class-name}")
    private String driverClassName;

    @Value("${app.datasource.second.url}")
    private String url;

    @Value("${app.datasource.second.username}")
    private String username;

    @Value("${app.datasource.second.password}")
    private String password;

    @Bean("second-datasource")
    public DataSource dataSource() {
        DataSourceProperties dataSourceProperties = new DataSourceProperties();
        dataSourceProperties.setDriverClassName(driverClassName);
        dataSourceProperties.setUrl(url);
        dataSourceProperties.setUsername(username);
        dataSourceProperties.setPassword(password);
        dataSourceProperties.setType(HikariDataSource.class);
        return dataSourceProperties.initializeDataSourceBuilder().build();
    }

    @Bean("second-entity-manager-factory-bean")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(EntityManagerFactoryBuilder entityManagerFactoryBuilder) {
        return entityManagerFactoryBuilder.dataSource(dataSource())
                                          .packages("com.vulinh.entity.second")
                                          .persistenceUnit("second")
                                          .build();
    }

    @Bean("second-transaction-manager")
    public PlatformTransactionManager platformTransactionManager(
        @Qualifier("second-entity-manager-factory-bean") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
