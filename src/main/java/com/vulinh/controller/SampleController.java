package com.vulinh.controller;

import com.vulinh.dto.BookDTO;
import com.vulinh.dto.StudentDTO;
import com.vulinh.service.BookService;
import com.vulinh.service.StudentService;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sample")
public class SampleController {

    private final BookService bookService;
    private final StudentService studentService;

    public SampleController(BookService bookService, StudentService studentService) {
        this.bookService = bookService;
        this.studentService = studentService;
    }

    @GetMapping("/all-student")
    public Object findAllStudent(Pageable pageable) {
        return studentService.findAllStudent(pageable);
    }

    @PostMapping("/create-student")
    public Object createStudent(@RequestBody StudentDTO studentDTO) {
        return studentService.createStudent(studentDTO);
    }

    @GetMapping("/all-book")
    public Object findAllBook(Pageable pageable) {
        return bookService.findAllBook(pageable);
    }

    @PostMapping("/create-book")
    public Object createBook(@RequestBody BookDTO bookDTO) {
        return bookService.createBook(bookDTO);
    }
}
