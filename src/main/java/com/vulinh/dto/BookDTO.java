package com.vulinh.dto;

import com.vulinh.entity.second.Book;

import java.time.LocalDate;

public class BookDTO {

    public static BookDTO toBookDTO(Book book) {
        if (book == null) {
            return null;
        }

        BookDTO bookDTO = new BookDTO();

        bookDTO.setId(book.getId());
        bookDTO.setName(book.getName());
        bookDTO.setAuthor(book.getAuthor());
        bookDTO.setPublishedDate(book.getPublishedDate());
        bookDTO.setPrice(book.getPrice());

        return bookDTO;
    }

    public static Book toBook(BookDTO bookDTO) {
        if (bookDTO == null) {
            return null;
        }

        Book book = new Book();

        book.setId(bookDTO.getId());
        book.setName(bookDTO.getName());
        book.setAuthor(bookDTO.getAuthor());
        book.setPublishedDate(bookDTO.getPublishedDate());
        book.setPrice(bookDTO.getPrice());

        return book;
    }

    private String id;
    private String name;
    private String author;
    private Long price;
    private LocalDate publishedDate;

    public BookDTO() {
    }

    public BookDTO(String id, String name, String author, Long price, LocalDate publishedDate) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.price = price;
        this.publishedDate = publishedDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public LocalDate getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(LocalDate publishedDate) {
        this.publishedDate = publishedDate;
    }

    @Override
    public String toString() {
        return "BookDTO{" +
            "id='" + id + '\'' +
            ", name='" + name + '\'' +
            ", author='" + author + '\'' +
            ", price=" + price +
            ", publishedDate=" + publishedDate +
            '}';
    }
}
