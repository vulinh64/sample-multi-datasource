package com.vulinh.repository.first;

import com.vulinh.entity.first.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, String> {
}
