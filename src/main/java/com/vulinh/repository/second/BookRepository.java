package com.vulinh.repository.second;

import com.vulinh.entity.second.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, String> {
}
