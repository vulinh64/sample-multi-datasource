package com.vulinh.service;

import com.vulinh.dto.BookDTO;
import com.vulinh.entity.second.Book;
import com.vulinh.repository.second.BookRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BookService {

    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Transactional
    public BookDTO createBook(BookDTO bookDTO) {
        if (bookDTO == null || bookDTO.getId() == null) {
            throw new IllegalArgumentException();
        }

        Book savedEntity = bookRepository.save(BookDTO.toBook(bookDTO));

        return BookDTO.toBookDTO(savedEntity);
    }

    public Page<BookDTO> findAllBook(Pageable pageable) {
        return bookRepository.findAll(pageable).map(BookDTO::toBookDTO);
    }
}
