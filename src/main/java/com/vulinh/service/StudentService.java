package com.vulinh.service;

import com.vulinh.dto.StudentDTO;
import com.vulinh.entity.first.Student;
import com.vulinh.repository.first.StudentRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Transactional
    public StudentDTO createStudent(StudentDTO studentDTO) {
        // Omit validation except ID
        if (studentDTO == null || studentDTO.getId() == null) {
            throw new IllegalArgumentException();
        }

        Student savedEntity = studentRepository.save(StudentDTO.toStudent(studentDTO));

        return StudentDTO.toStudentDTO(savedEntity);
    }

    public Page<StudentDTO> findAllStudent(Pageable pageable) {
        return studentRepository.findAll(pageable).map(StudentDTO::toStudentDTO);
    }
}
